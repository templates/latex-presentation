# Presentation title

The up-to-date version of the document is built in CI and resides as artifact.

> FIXME

- [Download the file](https://git.dbogatov.org/templates/latex-presentation/-/jobs/artifacts/master/raw/presentation.pdf?job=artifacts)
- [View the file](https://git.dbogatov.org/templates/latex-presentation/-/jobs/artifacts/master/file/presentation.pdf?job=artifacts)

## How to compile

```bash
./document/build.sh # to compile without notes
./document/build.sh -n # to compile wit notes
open ./document/dist/*.pdf # to open
```
